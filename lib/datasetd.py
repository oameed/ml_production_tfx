##############################################################
### PRODUCTION ML PIPELINES WITH TENSORFLOW EXTENDED (TFX) ###
### DATASET FUNCTION DEFINITIONS                           ###
### by: OAMEED NOAKOASTEEN                                 ###
##############################################################

def get_data(NAME):
 import os
 import tempfile
 import urllib.request
 DATA_ROOT=tempfile.mkdtemp(prefix='tfx-data')
 if NAME in ['classifier_penguin']:
  _data_url     = 'https://raw.githubusercontent.com/tensorflow/tfx/master/tfx/examples/penguin/data/labelled/penguins_processed.csv'   
  _data_filepath=os.path.join(DATA_ROOT, "data.csv")
 urllib.request.urlretrieve(_data_url, _data_filepath) 
 return _data_filepath, DATA_ROOT


