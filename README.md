# ML Production with [TensorFlow Extended (TFX)](https://www.tensorflow.org/tfx)

## References

[[1].](https://www.tensorflow.org/tfx/tutorials/tfx/penguin_simple) _Simple TFX Pipeline Tutorial using [Penguin dataset](https://allisonhorst.github.io/palmerpenguins/articles/intro.html)_  

_Useful_

[[2].](https://www.youtube.com/playlist?list=PLQY2H8rRoyvxR15n04JiW0ezF5HQRs_8F) _TensorFlow on YouTube, TFX Playlist_  

## Code Statistics
<pre>
github.com/AlDanial/cloc v 1.82  T=0.02 s (295.2 files/s, 20320.8 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
YAML                             1              0              0            229
Python                           4             42             24            110
Bourne Shell                     1              4              0              4
-------------------------------------------------------------------------------
SUM:                             6             46             24            343
-------------------------------------------------------------------------------
</pre>

## How to Run

* This project uses `TFX` Version `1.8.0`. The `YAML` files for creating the conda environments used to run this project are included in `run/conda`  

* on Local PC:
  1. `cd` to main project directory
  2. `./run/sh/train_classifier_penguin.sh`


