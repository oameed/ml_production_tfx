##############################################################
### PRODUCTION ML PIPELINES WITH TENSORFLOW EXTENDED (TFX) ###
### CREATE PIPELINE                                        ###
### by: OAMEED NOAKOASTEEN                                 ###
##############################################################

import                     os
import                     sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'))
import tensorflow       as tf
from tfx      import v1 as tfx
from absl     import       logging
from paramd   import      (PIPELINE_NAME    ,
                           PIPELINE_ROOT    ,
                           METADATA_PATH    ,
                           SERVING_MODEL_DIR )
from datasetd import       get_data

logging.set_verbosity(logging.INFO)

_trainer_module_file      = os.path.join('..','..','lib','trainer_modules',PIPELINE_NAME+'.py')
_data_filepath, DATA_ROOT = get_data    (PIPELINE_NAME)


def _create_pipeline(pipeline_name    : str, 
                     pipeline_root    : str, 
                     data_root        : str,
                     module_file      : str, 
                     serving_model_dir: str,
                     metadata_path    : str ) -> tfx.dsl.Pipeline:
 
 filesystem  = tfx.proto.PushDestination.Filesystem(base_directory=serving_model_dir)
  
 example_gen = tfx.components.CsvExampleGen(input_base=data_root)                             # Brings data into the pipeline.
 
 trainer     = tfx.components.Trainer(module_file     =module_file                       ,    # Uses user-provided Python function that trains a model.
                                      examples        =example_gen.outputs['examples']   ,
                                      train_args      =tfx.proto.TrainArgs(num_steps=100),
                                      eval_args       =tfx.proto.EvalArgs (num_steps=5  ) )
                                                                                              # Pushes the model to a filesystem destination.
 pusher      = tfx.components.Pusher (model           =trainer.outputs['model']                        ,
                                      push_destination=tfx.proto.PushDestination(filesystem=filesystem) )
 
 components  = [example_gen,trainer,pusher]                                                   # These three components will be included in the pipeline.
 
 return tfx.dsl.Pipeline(pipeline_name             =pipeline_name,
                         pipeline_root             =pipeline_root,
                         metadata_connection_config=tfx.orchestration.metadata.sqlite_metadata_connection_config(metadata_path),
                         components                =components    )


tfx.orchestration.LocalDagRunner().run(_create_pipeline(pipeline_name    =PIPELINE_NAME       ,
                                                        pipeline_root    =PIPELINE_ROOT       ,
                                                        data_root        =DATA_ROOT           ,
                                                        module_file      =_trainer_module_file,
                                                        serving_model_dir=SERVING_MODEL_DIR   ,
                                                        metadata_path    =METADATA_PATH        ))


