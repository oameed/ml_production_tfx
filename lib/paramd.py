##############################################################
### PRODUCTION ML PIPELINES WITH TENSORFLOW EXTENDED (TFX) ###
### PARAMETER DEFINITIONS                                  ###
### by: OAMEED NOAKOASTEEN                                 ###
##############################################################

import os
import argparse

### DEFINE PARSER
parser=argparse.ArgumentParser() 

### DEFINE PARAMETERS
parser.add_argument('-n', type=str, help="output directory", required=True)

### ENABLE FLAGS
args=parser.parse_args()

PIPELINE_NAME    =args.n
PIPELINE_ROOT    =os.path.join('..','..','pipelines'    , args.n               )
METADATA_PATH    =os.path.join('..','..','metadata'     , args.n, 'metadata.db')
SERVING_MODEL_DIR=os.path.join('..','..','serving_model', args.n               )


